# QM XC7A35T DDR3 board repository

## Abstract

Repository containing the board definition of QMTECH Artix 7 XC7A35T DDR3 CoreBoard + DaughterBoard. Composed using official schemes and examples from QMTECH repository.

**I DECLINE EVERY RESPONSIBILITY. USE IT AT YOUR OWN RISK.**

## Files description

* **constraints**
    * mig_ddr3.xdc:constraint files for DDR3
    * qm-xc7a35t-ddr3.xdc: general board constraint files
* **qm-xc7a35t-ddr3**
    * **1.0**
        * board.xml: board definition
        * mig.prj: MIG 7 Series memory definition
        * part0_pins.xml: physical I/O definition
        * preset.xml: IP presets

# Notes

During the analysis of board schematics and example files, the following **discrepancies** were noted:

- memory examples uses MT41**J**128M16JT-125 as DDR3 Memory Part, whereas in schematics and on the actual board there is MT41**K**128M16JT-125 DDR3L Memory Part
- inside the core board schematic, port E6 is both used by the onboard led D3 and by SDIO clock signal. Further analysis are required
