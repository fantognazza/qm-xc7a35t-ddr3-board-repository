### 50 MHz oscillator ###
#set_property -dict { IOSTANDARD LVCMOS33   PACKAGE_PIN N11  } [get_ports clk]

### 5 daughterboard leds ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R6  } [get_ports {led[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T5  } [get_ports {led[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R7  } [get_ports {led[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T7  } [get_ports {led[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R8  } [get_ports {led[4]}]

### 5 daughterboard pushbuttons ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B7  } [get_ports {btn[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M6  } [get_ports {btn[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N6  } [get_ports {btn[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R5  } [get_ports {btn[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P6  } [get_ports {btn[4]}]

### QSPI flash ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L12 } [get_ports qspi_cso]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J13 } [get_ports {qspi_dq[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J14 } [get_ports {qspi_dq[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K15 } [get_ports {qspi_dq[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K16 } [get_ports {qspi_dq[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L12 } [get_ports qspi_cclk]

### Onboard SW1 button ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L9  } [get_ports sw1]

### Onboard SW2 button ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K5  } [get_ports rst_n]

### 3 digits daughterboard seven segments display ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T10 } [get_ports {led_an[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K13 } [get_ports {led_an[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P11 } [get_ports {led_an[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R11 } [get_ports {led_an[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R10 } [get_ports {led_an[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N9  } [get_ports {led_an[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K12 } [get_ports {led_an[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P9  } [get_ports {led_an[7]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T9  } [get_ports {led_disp[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P10 } [get_ports {led_disp[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T8  } [get_ports {led_disp[2]}]

### Onboard SW1 button ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T15 } [get_ports uart_rx]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T14 } [get_ports uart_tx]

### GMII ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C4  } [get_ports eth_rstn]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B2  } [get_ports eth_gtx_clk]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C2  } [get_ports eth_tx_en]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN A4  } [get_ports eth_tx_clk]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C7  } [get_ports eth_tx_er]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C3  } [get_ports {eth_tx[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN D4  } [get_ports {eth_tx[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN A3  } [get_ports {eth_tx[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B4  } [get_ports {eth_tx[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN A5  } [get_ports {eth_tx[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN D5  } [get_ports {eth_tx[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN D6  } [get_ports {eth_tx[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C6  } [get_ports {eth_tx[7]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN F3  } [get_ports eth_rx_dv]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN F5  } [get_ports eth_rx_clk]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B1  } [get_ports eth_rx_er]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN F4  } [get_ports {eth_rx[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E1  } [get_ports {eth_rx[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN F2  } [get_ports {eth_rx[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E5  } [get_ports {eth_rx[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN D3  } [get_ports {eth_rx[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E3  } [get_ports {eth_rx[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN D1  } [get_ports {eth_rx[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E2  } [get_ports {eth_rx[7]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN C1  } [get_ports eth_col]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN A2  } [get_ports eth_crs]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN G4  } [get_ports eth_mdc]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN G5  } [get_ports eth_mdio]

### Daughterboard J10 PMOD ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P13 } [get_ports {j10[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T13 } [get_ports {j10[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T12 } [get_ports {j10[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L13 } [get_ports {j10[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P14 } [get_ports {j10[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R13 } [get_ports {j10[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R12 } [get_ports {j10[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N12 } [get_ports {j10[7]}]

### Daughterboard J11 PMOD ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M12 } [get_ports {j11[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N14 } [get_ports {j11[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P15 } [get_ports {j11[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R15 } [get_ports {j11[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N13 } [get_ports {j11[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N16 } [get_ports {j11[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P16 } [get_ports {j11[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R16 } [get_ports {j11[7]}]

### Onboard D3 led ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E6  } [get_ports led_d3]

### Daughterboard SD card ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K5  } [get_ports sdio_cmd]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B5  } [get_ports {sdio_dat[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN B6  } [get_ports {sdio_dat[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J4  } [get_ports {sdio_dat[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J5  } [get_ports {sdio_dat[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN E6  } [get_ports sdio_clk]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN A7  } [get_ports sdio_cdn]

### Daughterboard JP1 connector ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M1  } [get_ports {jp1[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M2  } [get_ports {jp1[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P1  } [get_ports {jp1[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N1  } [get_ports {jp1[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P3  } [get_ports {jp1[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P4  } [get_ports {jp1[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N4  } [get_ports {jp1[6]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M5  } [get_ports {jp1[7]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R1  } [get_ports {jp1[8]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R2  } [get_ports {jp1[9]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T2  } [get_ports {jp1[10]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN R3  } [get_ports {jp1[11]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T3  } [get_ports {jp1[12]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN T4  } [get_ports {jp1[13]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN P5  } [get_ports {jp1[14]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L5  } [get_ports {jp1[15]}]

### Daughterboard VGA ###
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N11 } [get_ports vga_clk]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N2  } [get_ports {vga_red[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN M4  } [get_ports {vga_red[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN N3  } [get_ports {vga_red[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K2  } [get_ports {vga_red[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L4  } [get_ports {vga_red[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K3  } [get_ports {vga_green[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN H3  } [get_ports {vga_green[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN H4  } [get_ports {vga_green[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J3  } [get_ports {vga_green[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L2  } [get_ports {vga_green[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN H5  } [get_ports {vga_green[5]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN L3  } [get_ports {vga_blue[0]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN K1  } [get_ports {vga_blue[1]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN J1  } [get_ports {vga_blue[2]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN H2  } [get_ports {vga_blue[3]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN H1  } [get_ports {vga_blue[4]}]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN G1  } [get_ports vga_hsync]
#set_property -dict { IOSTANDARD LVCMOS33    PACKAGE_PIN G2  } [get_ports vga_vsync]

### PROJECT ###
#set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
